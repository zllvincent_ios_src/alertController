//
//  AppDelegate.h
//  AlertControllerTest
//
//  Created by Vincent on 2017/2/25.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

