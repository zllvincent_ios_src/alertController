//
//  ViewController.m
//  AlertControllerTest
//
//  Created by Vincent on 2017/2/25.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import "ViewController.h"
#import "ZLLAlertController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    NSArray *arr = @[@"actionSheet",@"alert"];
    for(int i=0;i<arr.count;i++){
        UIButton *btn = [UIButton new];
        btn.tag = 1000+i;
        btn.frame = CGRectMake(i*self.view.center.x+20, 100, self.view.center.x-40, 40);
        btn.backgroundColor = [UIColor purpleColor];
        [btn setTitle:arr[i] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnSender:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
}

-(void)btnSender:(UIButton *)btn{
    if(btn.tag==1000){
        [ZLLAlertController alertPreferredStyle:UIAlertControllerStyleActionSheet title:@"actionSheet" message:@"测试" actionTitles:@[@"test1",@"test2",@"test3"] actionStyles:@[@1,@0,@0] action:^(UIAlertAction *alertAction) {
            
            
            
        }];
    }
    else if(btn.tag==1001){
        [ZLLAlertController alertPreferredStyle:UIAlertControllerStyleAlert title:@"Alert" message:@"测试" actionTitles:@[@"test1",@"test2",@"test3"] actionStyles:@[@1,@2,@0] action:^(UIAlertAction *alertAction) {
            
            
            
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
