//
//  ZLLAlertController.h
//  AlertDemo
//
//  Created by Vincent on 2017/1/18.
//  Copyright © 2017年 zhihua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLLAlertController : UIAlertController

/*
 *警告视图控制器(快速创建UIAlertController）
 *@param    alertStyle
 *@param    title       警告的标题
 *@param    msg         警告的信息
 *@param    actTitles   UIAlertAction的标题数组
 *@param    actStyles   UIAlertActionStyle 封装的NSNumber对象数组
 *@param    handler     点击相关UIAlertAction的索引代码块
 *
 */
+(void)alertPreferredStyle:(UIAlertControllerStyle)alertStyle title:(NSString *)title message:(NSString *)msg actionTitles:(NSArray<NSString *> *)actTitles actionStyles:(NSArray<NSNumber *> *)actStyles action:(void (^)(UIAlertAction *alertAction))handler;




@end
