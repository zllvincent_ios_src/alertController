//
//  ZLLAlertController.m
//  AlertDemo
//
//  Created by Vincent on 2017/1/18.
//  Copyright © 2017年 zhihua. All rights reserved.
//

#import "ZLLAlertController.h"

@interface ZLLAlertController ()



@end

@implementation ZLLAlertController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    
}

+(void)alertPreferredStyle:(UIAlertControllerStyle)alertStyle title:(NSString *)title message:(NSString *)msg actionTitles:(NSArray<NSString *> *)actTitles actionStyles:(NSArray<NSNumber *> *)actStyles action:(void (^)(UIAlertAction *))handler{
    
    ZLLAlertController *alert = [ZLLAlertController alertControllerWithTitle:title message:msg preferredStyle:alertStyle];
    
    for(int i=0;i<actTitles.count;i++){
        NSString *str = actTitles[i];
        int actStyle = [actStyles[i] intValue];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:str style:actStyle handler:^(UIAlertAction * _Nonnull action) {
            
            handler(action);
            
        }];
        
        [alert addAction:action];
    }
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}







/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
